//package com.example.jdbc.repo;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Repository;
//
//import com.example.jdbc.entity.Student;
//
//@Repository
//public class StudentDAOService implements StudentRepo {
//
//	public List<Student> db = new ArrayList<Student>();
//
//	@Override
//	public Student save(Student student) {
//		db.add(student);
//		return student;
//	}
//
//	@Override
//	public Optional<Student> findById(int id) {
//		return db.stream().filter(stud -> stud.getId().equals(id)).findFirst();
//
//	}
//
//	@Override
//	public Student deleteById(int id) {
//		Optional<Student> maybe = findById(id);
//		if (maybe.isEmpty())
//			return null;
//		db.remove(maybe.get());
//		return maybe.get();
//
//	}
//
//	@Override
//	public List<Student> findAll() {
//		return db;
//
//	}
//
//	@Override
//	public int updateStudent(int id, Student student) {
//		return findById(id).map(stud -> {
//			int indexToDelete = db.indexOf(stud);
//			if (indexToDelete >= 0) {
//				db.set(indexToDelete, new Student(student.getId(), student.getFirstName(), student.getLastName()));
//				return 1;
//			}
//			return 0;
//
//		}).orElse(0);
//	}
//}
