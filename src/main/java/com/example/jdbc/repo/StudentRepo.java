package com.example.jdbc.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.jdbc.entity.Student;

@Repository
public interface StudentRepo {
	
	
	public Integer save(Student student);
	public Student findById(int id);
	public boolean deleteById(int id);
	public List<Student> findAll();
	public void updateStudent(int id, Student student);

}
