package com.example.jdbc.repo;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.jdbc.entity.Student;

@Repository
public class StudentJdbcService implements StudentRepo {

	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Override
	public Integer save(Student student) {
		String query = "insert into students (first_name, last_name)" + "values(?, ?)";
		int res = jdbcTemplate.update(query, student.getFirstName(), student.getLastName());
		return res;
	}

	@Override
	public Student findById(int id) {

		String sql = "select * from students where id=?";
		Student student = (Student) jdbcTemplate.queryForObject(sql, (resultSet, i) -> new Student(resultSet.getInt("id"),
				resultSet.getString("first_name"), resultSet.getString("last_name")),id);
		return student;
		
	}

	@Override
	public boolean deleteById(int id) {
		String query = "delete from students where id=?";
		jdbcTemplate.update(query, id);
		return true;
	}

	@Override
	public List<Student> findAll() {
		String sql = "select * from students";

		return jdbcTemplate.query(sql, (resultSet, i) -> new Student(resultSet.getInt("id"),
				resultSet.getString("first_name"), resultSet.getString("last_name")));
	}


	@Override
	public void updateStudent(int id, Student student) {

		String sql = "update students set first_name = ?, last_name = ? where id = ?";
	    
	    Student updated = findById(id);
		updated.setFirstName(student.getFirstName());
		updated.setLastName(student.getLastName());
		Object[] params = {updated.getFirstName(),updated.getLastName(), id};
	    jdbcTemplate.update(sql, params);
		
	}

}
