package com.example.jdbc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jdbc.entity.Student;
import com.example.jdbc.service.StudentService;

@RequestMapping("/stds")
@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService;


	@PostMapping
	public Integer save(@RequestBody Student student) {
		return studentService.save(student);
	}

	@GetMapping("/{id}")
	public Student findById(@PathVariable int id) {
		return studentService.findById(id);
	}

	@DeleteMapping("/{id}")
	public boolean deleteById(@PathVariable("id") int id) {
		studentService.deleteById(id);
		return true;
	}

	@GetMapping
	public List<Student> findAll() {
		return studentService.findAll();
	}

	@PutMapping("/{id}")
	public void updateStudent(@PathVariable int id,@RequestBody Student student) {
		studentService.updateStudent(id, student);
	} 

}
