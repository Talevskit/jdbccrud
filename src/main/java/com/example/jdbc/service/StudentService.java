package com.example.jdbc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.jdbc.entity.Student;
import com.example.jdbc.repo.StudentRepo;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepo studentRepo;
	
	
	public Integer save(Student student) {
		studentRepo.save(student);
		return 1;
	}
	public Student findById(int id) {
		return studentRepo.findById(id);
	}
	public boolean deleteById(int id) {
		studentRepo.deleteById(id);
		return true;
	}
	public List<Student> findAll() {
		return studentRepo.findAll();
	}
	
	public void updateStudent(int id, Student student) {
		studentRepo.updateStudent(id, student);
	}
}
